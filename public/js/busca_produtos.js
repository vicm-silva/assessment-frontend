//Função para busca os produtos das categorias na API

function requisicaoDeProdutos(id){

    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:8888/api/V1/categories/' + id);

    xhr.addEventListener('load', function(){
        var resposta = xhr.responseText;
        var produtos = JSON.parse(resposta);
        var produto = produtos['items'];

        for(i = 0; i < produto.length; i++){
            var PP = produto[i];
            var name = PP['name'];
            var price = PP['price'];
            var image = PP['image'];
            var path = PP['path'];

            var criaProdutos = adicionaProdutosPage(name, price, image, path);
        }
    });

    xhr.send();
}

//functions para criar as divs dos produtos buscados na api

function adicionaProdutosPage(name, price, image, path){
    var products = document.querySelector('.product__items'); 
    products.appendChild(criandoDivProduto(name, price, image, path));

    return products
}

function criandoDivProduto(content, preço, caminho, description){
    var divPai = document.createElement('div');
    divPai.classList.add('product__items-products');
    divPai.appendChild(criaDivImg(caminho, description));
    divPai.appendChild(montaPagrafoConteudo(content));
    divPai.appendChild(montaPagrafoPreco(preço));
    divPai.appendChild(criabutton());

    return divPai
}

function criaDivImg(caminho, description){
    var div = document.createElement('div');
    div.classList.add('product__items-img');
    div.appendChild(montaImage(caminho, description));

    return div
}

function montaImage(caminho, description){
    var img = document.createElement('img');
    img.classList.add('product__img-product');
    img.src = caminho;
    img.alt = description;

    return img
}

function montaPagrafoConteudo (content){
    var paragrafo = document.createElement('p');
    paragrafo.classList.add('product__name');
    paragrafo.textContent = content;

    return paragrafo
}

function montaPagrafoPreco (preço){
    var paragrafoPrice = document.createElement('p');
    paragrafoPrice.classList.add('product__price');
    paragrafoPrice.textContent = 'R$' + preço;

    return paragrafoPrice
}

function criabutton (){
    var button = document.createElement('button');
    button.classList.add('product__button');
    button.textContent = 'COMPRAR';

    return button
}