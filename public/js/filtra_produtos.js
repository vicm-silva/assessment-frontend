// Filta os produtos de acordo com a escrita no input

var campoFiltro = document.querySelector('.search__input');

campoFiltro.addEventListener('input', function () {

  var produtos = document.querySelectorAll('.product__items-products');

  if (this.value.length > 0) {
    for (var i = 0; i < produtos.length; i++) {
        var produto = produtos[i];
        var nome = produto.querySelector('.product__name');
        var name = nome.textContent;
        var expressao = new RegExp(this.value, 'i');

        if (!expressao.test(name)) {
          produto.style.display = 'none';
        } else {
          produto.style.display = 'block';
        }
    }
  } else {
    for (var i = 0; i < produto.length; i++) {
        var produto = produtos[i];
        produto.style.display = 'block';
    }
  }
});