/*Quando carregamos a página no breakpoint de 320px, 
ela esconde o menu de navegação e ao clicar no icone menu-aberto 
o menu navegação abre novamente*/
var image =  document.querySelector('.header__menu-aberto')
var menuNav = document.querySelector('.header__nav')

if(window.matchMedia("(max-width:320px)").matches){
    menuNav.style.display = 'none';
} else {
    menuNav.style.display = 'block';
}

image.addEventListener('click', function(){
    if(menuNav.style.display === 'none'){
        menuNav.style.display = 'block';
    }else{
        menuNav.style.display = 'none';
    }
})

/*Quando carregamos a página no breakpoint de 320px, 
ela esconde o input de busca e ao clicar no icone lupa
o input aparece novamente*/

var imageLupa =  document.querySelector('.header__img-lupa')
var input = document.querySelector('.header__search')

if(window.matchMedia("(max-width:320px)").matches){
    input.style.display = 'none';
} else {
    input.style.display = 'block';
}

imageLupa.addEventListener('click', function(){
    if(input.style.display === 'none'){
        input.style.display = 'block';
    }else{
        input.style.display = 'none';
    }
})