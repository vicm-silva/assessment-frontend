/*Busca as categorias na api para crias as tags, assim criando o menu de navegação*/
window.onload = function(){
    var xml = new XMLHttpRequest();
    xml.open('GET', 'http://localhost:8888/api/V1/categories/list');
    
    xml.addEventListener('load', function(){
        var resposta = xml.responseText;
        var categorias = JSON.parse(resposta);
        var link = categorias['items'];
    
        var hrefContato = '#';
        var dadoContato = 'Contato';
    
        var hrefSapatos = 'sapatos.html';
        var hrefCalças = 'calcas.html';
        var hrefCamisetas = 'camisetas.html';
    
        for(i = 0; i < link.length; i++){
            var links = link[i];
            var name= links['name'];
                
            if(name == 'Camisetas'){
                var adicionaLink = incluiNaNav(name, hrefCamisetas);
            } else if (name == 'Calças'){
                    var adicionaLink = incluiNaNav(name, hrefCalças);
                } else {
                    var adicionaLink = incluiNaNav(name, hrefSapatos); 
                }
            }

            incluiNaNav(dadoContato, hrefContato);
        
    });
        
    xml.send();
}

//Funções para criar as tags <a> e <li> para inclui-los na <ul> presente na <nav>
function incluiNaNav(valor, href){
    var nav = document.querySelector('.menu__list');
    nav.appendChild(criandoLi(valor, href));

    return nav
}

function criandoLi(conteudo, href,){
    var li = document.createElement('li');
    li.classList.add('menu__links');
    li.appendChild(criaLink(conteudo, href));

    return li 
}

function criaLink(conteudo, href){
    var a = document.createElement('a');
    a.classList.add('menu__link');
    a.textContent = conteudo;
    a.href = href;

    return a
}